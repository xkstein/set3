function [L2Error,H1Error] = L2H1Error(ksi, h)
    u = @(x) sin(pi*x);
    du = @(x) pi*cos(pi*x);
    u_uh = @(x,i) u(x) - ksi(i);
    du_uh = @(x,i) du(x) - (ksi(i+1)-ksi(i-1))/(2*h);

    L2Sum = u_uh(0,1) + u_uh(1,length(ksi));
    for i = 2:length(ksi)-1
        x = h*(i-1);
        L2Sum = L2Sum + 2 * (u_uh(x,i))^2;
    end
    L2Error = sqrt(h*L2Sum/2);

    H1Sum = ((ksi(2)-ksi(1))/h) ^ 2 + ((ksi(end)-ksi(end-1))/2)^2;
    for i = 2:length(ksi)-1
        x = h*(i-1);
        H1Sum = H1Sum + 2 * (u_uh(x,i)^2 + du_uh(x,i)^2);
    end
    H1Error = sqrt(h*H1Sum/2);
end
