function main(n)
    close all

    h = 1/(n+1);
    x_i = @(i) h*(i);
    
    % note: time node 1 is at t = 0 and node m is at t=1
    m = n;
    dt = 1/(m);
    t_i = @(i) dt*i;

    u = @(x,t) sin(2*pi*x)*cos(2*pi*t);
    u0 = @(x) sin(2*pi*x);
    f = @(x,t) -2*pi*sin(2*pi*x)*sin(2*pi*t) + 4*pi^2*sin(2*pi*x)*cos(2*pi*t);

    % Making the stiffness matrix
    localA = [[1 -1]
          [-1 1]];

    A = zeros(n);
    A(1,1) = 1;
    A(n,n) = 1;
    for i = 1:n-1
        A(i:i+1,i:i+1) = A(i:i+1,i:i+1) + localA;
    end
    A = A/h;

    % Making the mass matrix
    M = zeros(n);
    localM = h*[[2/3 1/6]
              [1/6 0]];
    M(n,n) = 2*h/3;
    for i = 1:n-1
        M(i:i+1,i:i+1) = M(i:i+1,i:i+1) + localM;
    end

    b = zeros([n m+1]);

    ksi = zeros([n m+1]);
    refksi = zeros([n m+1]);

    for l = 0:m
        t = l * dt;

        for i = 1:n
            b(i,l+1) = f(x_i(i),t)*h;
            refksi(i,l+1) = u(x_i(i),t);
        end
    end
   
    % Populating u(x,0)
    for i = 1:n
        ksi(i,1) = u0(x_i(i));
    end

    for l = 1:m
        t = l * dt;

        %ksi = FWDEuler(ksi,l,M,A,b(:,l),dt);

        ksi = BWDEuler(ksi,l,M,A,b(:,l+1),dt);
    end

    % Function for finding the L2 and H1 error
    [L2,H1] = L2H1Error(ksi(:,3), h);
    fprintf('| 1/%i | %f | %f |\n',n+1,L2,H1);

    %X = 0:h:1;
    %plot(X,ksi)

    clear;
end

function ksi = BWDEuler(ksi,l,M,A,b,dt)
    bwd1 = (M + dt*A);
    bwd2 = M*ksi(:,l) + dt*b;

    ksi(:,l+1) = linsolve(bwd1,bwd2);
end

function ksi = FWDEuler(ksi,l,M,A,b,dt)
    fwd1 = M*ksi(:,l) - dt*A*ksi(:,l) + dt*b;
    ksi(:,l+1) = linsolve(M,fwd1);
end

function ksi = crankNic(ksi,l,M,A,b,dt)

end
