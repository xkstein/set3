function[L2Error,H1Error] = findError(KSI,X,Y,h)

    u = @(x,y) sin(pi*x).*sin(pi*y);
    dux = @(x,y) pi*cos(pi*x).*sin(pi*y);
    duy = @(x,y) pi*sin(pi*x).*cos(pi*y);

    interpKSI = interpolateMatrix(KSI);
    n = length(X);
    en = 2*n-1;
    h = h/2;
    [KSIy,KSIx] = gradient(interpKSI,h);
    %en = n;
    %[KSIy,KSIx] = gradient(KSI,h);

    u_uh = zeros(en);
    u_uhx = zeros(en);
    u_uhy = zeros(en);
    du_uh = zeros(en);

    for j = 1:en
        y = (j-1)*h;
        for i = 1:en
            x = (i-1)*h;

            %u_uh(i,j) = u(x,y) - KSI(i,j);
            u_uh(i,j) = u(x,y) - interpKSI(i,j);
            u_uhx(i,j) = dux(x,y) - KSIx(i,j);
            u_uhy(i,j) = duy(x,y) - KSIy(i,j);
        end
    end

    L2Error = sqrt(simpson(u_uh.^2));
    H1Error = sqrt(abs(simpson(u_uh + u_uhx.^2 + u_uhy.^2)));
end

