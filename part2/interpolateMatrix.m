function expandedMatrix = interpolateMatrix(inMatrix)
    n = size(inMatrix,1);
    en = 2*n-1;
    expandedMatrix = zeros(en);
    for i = 1:n
        for j = 1:n
            expandedMatrix((2*i-1),(2*j-1)) = inMatrix(i,j);
        end
    end

    for j = 1:en
        if mod(j,2) == 1
            for i = 2:2:en-1
                expandedMatrix(i,j) = avgz(expandedMatrix(i+1,j),expandedMatrix(i-1,j));
            end
        else
            for i = 1:2:en
                expandedMatrix(i,j) = avgz(expandedMatrix(i,j+1),expandedMatrix(i,j-1));
            end
        end
    end
   
    for j = 2:2:en-1
        for i = 2:2:en-1
            expandedMatrix(i,j) = avgz(expandedMatrix(i+1,j),expandedMatrix(i-1,j));
        end
    end

end

function outz = avgz(z1,z2)
    outz = (z2-z1)/2 + z1;
end
