function[node] = loc2glo(tri,local,n)
    row = floor((tri-1)/(2*(n+1)));
    col = floor(mod((tri-1)/2, n+1));

    if (mod(tri,2) ~= 0)
        switch (local)
            case 1
                row = row + 1;
            case 3
                row = row + 1;
                col = col + 1;
        end
    else
        switch (local)
            case 1
                col = col + 1;
            case 2
                row = row + 1;
                col = col + 1;
        end
    end
    
    if (row == 0 || col == 0 || row == n+1 || col == n+1)
        node = 0;
        return
    end
    node = ((row - 1) * n) + col;
end
