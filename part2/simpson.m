function integral = simpson(varargin)
    switch nargin
        case 1
            % Integral of points
            F = varargin{1};
            n = size(F,1);
            h = 1/(n-1);
        case 2
            % Function then n
            f = varargin{1};
            n = varargin{2};
            h = 1/(n-1);
            F = zeros(n);
            for i = 1:n
                for j = 1:n
                    x = (i-1)*h;
                    y = (j-1)*h;
                    F(i,j) = f(x,y);
                end
            end
    end

    if mod(n,2) == 0
        error("The mesh must have an odd number of nodes in either direction")
        return
    end

    simpsonAxis = ones(n,1);
    for i = 2:n-1
        if mod(i,2) == 1
            simpsonAxis(i) = 2;
        else
            simpsonAxis(i) = 4;
        end
    end
    simpson = simpsonAxis.*transpose(simpsonAxis);

    simpsonSum = 0;
    for i = 1:n
        for j = 1:n
            simpsonSum = simpsonSum + simpson(i,j) * F(i,j);
        end
    end
    integral = simpsonSum * h*h/9;
end
